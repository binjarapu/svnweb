package model;

import java.util.ArrayList;

public class Author {
	private String name;
	private ArrayList<Commit> commits;
	public void setName(String name){
		this.name=name;
		}
	public String getName(){
		return name;
		}
	public void setCommits(ArrayList<Commit> commits){
		this.commits=commits;
		}
	public ArrayList<Commit> getCommits(){
		return commits;
		}
}