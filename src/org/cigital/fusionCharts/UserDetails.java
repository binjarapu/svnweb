package org.cigital.fusionCharts;
/**
 * 
 * @author VINOD.K
 *
 */
public class UserDetails {
	private String username,password;
	private int userid;
	public UserDetails(int userid,String username,String password){
		this.userid=userid;
		this.username=username;
		this.password=password;
	}
	public UserDetails(){
		
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	
}
