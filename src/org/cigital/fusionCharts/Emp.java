package org.cigital.fusionCharts;

import java.util.Date;

import javax.persistence.Id;

import java.sql.Blob;

public class Emp {
	   @Id
	   private int diffId;
	   private String revisionId; 
	   private Blob code;   
	   private int numOfAddedLines;  
	   private int numOfDeletedLines;  
	   private int userId;
	   private Date date;
	   private char mode;
	   private String filePath;
	   public Emp(){}
		public Emp( String revisionId, Blob code, int numOfAddedLines,
			int numOfDeletedLines, int userId, Date date, char mode,
			String filePath) 
		{
		this.revisionId = revisionId;
		this.code = code;
		this.numOfAddedLines = numOfAddedLines;
		this.numOfDeletedLines = numOfDeletedLines;
		this.userId = userId;
		this.date = date;
		this.mode = mode;
		this.filePath = filePath;
			}
		public int getDiffId() {
			return diffId;
		}
		public void setDiffId(int diffId) {
			this.diffId = diffId;
		}
		public String getRevisionId() {
			return revisionId;
		}
		public void setRevisionId(String revisionId) {
			this.revisionId = revisionId;
		}
		public Blob getCode() {
			return code;
		}
		public void setCode(Blob code) {
			this.code = code;
		}
		public int getNumOfAddedLines() {
			return numOfAddedLines;
		}
		public void setNumOfAddedLines(int numOfAddedLines) {
			this.numOfAddedLines = numOfAddedLines;
		}
		public int getNumOfDeletedLines() {
			return numOfDeletedLines;
		}
		public void setNumOfDeletedLines(int numOfDeletedLines) {
			this.numOfDeletedLines = numOfDeletedLines;
		}
		public int getUserId() {
			return userId;
		}
		public void setUserId(int userId) {
			this.userId = userId;
		}
		public Date getDate() {
			return date;
		}
		public void setDate(Date date) {
			this.date = date;
		}
		public char getMode() {
			return mode;
		}
		public void setMode(char mode) {
			this.mode = mode;
		}
		public String getFilePath() {
			return filePath;
		}
		public void setFilePath(String filePath) {
			this.filePath = filePath;
		}
   
	}