package org.cigital.fusionCharts;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;

import com.cigital.CodeChange.SvnObject;

public class GetLoccFromDatabase {
	HashMap<String,Integer> userdetails=new HashMap<String,Integer>();
   private SessionFactory factory; 
   String uid;
	  String sdate;
	  String edate;
	  int lr;
   public GetLoccFromDatabase(String s1,String s2,String s3,int lr){
	   this.uid=s1;
	   String[] tmp=s2.split("/");
	   s2=tmp[2]+tmp[0]+tmp[1];
	   this.sdate=s2;
	   tmp=s3.split("/");
	   s3=tmp[2]+tmp[0]+tmp[1];
	   this.edate=s3;
	   factory  = new AnnotationConfiguration()
	      .configure("hibernate.cfg.xml").buildSessionFactory();
	      Session session = factory.openSession();
	//   createusermap();
	   this.uid=s1;
	   this.lr=lr;
   }
   
 /*  private void createusermap() {
		// TODO Auto-generated method stub
		 Session session = factory.openSession();
	      Transaction tx = null;
	      try{
	         tx = session.beginTransaction();
	         List users = session.createQuery("FROM UserDetails").list();
	         for(Iterator itr=users.iterator();itr.hasNext();){
	        	 UserDetails ud=(UserDetails) itr.next();
	        	// pw.println("<p>"+ud.getUserid()+" "+ud.getUsername()+" "+ud.getPassword()+"</p>");
	        	 userdetails.put(ud.getUsername(), ud.getUserid());
	         }
	         tx.commit();
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	      }finally {
	         session.close();
	      }	
	}  */
	public String main1(){ 
	  factory  = new AnnotationConfiguration()
      .configure("hibernate.cfg.xml").buildSessionFactory();
      Session session = factory.openSession();
      Transaction tx = null;
      ArrayList<String> dates = new ArrayList<String>();
      ArrayList<Integer> added = new ArrayList<Integer>();
      ArrayList<Integer> removed = new ArrayList<Integer>();
      ArrayList<String> filePath=new ArrayList<String>();
      ArrayList<String> newDates = new ArrayList<String>();
  
      String s;
      int p;
      String f;
      ArrayList<String> lines = new ArrayList<String>();
      TreeMap<String,TreeMap<String, Integer>> map=new TreeMap<>();
      TreeMap<String,TreeMap<String, Integer>> map2=new TreeMap<>();
      try{
          tx = session.beginTransaction();
          String hql;
        if(uid.length()>0)
          hql= "from SvnObject where user= '" + uid+ "' and date between :from and :to ";   
        else
        	hql= "from SvnObject where  date between :from and :to "; 
         Query query = session.createQuery(hql);
         query.setString("from",sdate);
         query.setString("to",edate);   
         
         List Emps = query.list();  
         for (@SuppressWarnings("rawtypes")
		Iterator iterator = 
                           Emps.iterator(); iterator.hasNext();){
            SvnObject emp = (SvnObject) iterator.next(); 
            dates.add(emp.getDate().toString());
            added.add(emp.getNumOfAddedLines());
            removed.add(emp.getNumOfDeletedLines());
            filePath.add(emp.getFilepath().toString());
            s = emp.getDate().toString() +"|"+emp.getNumOfAddedLines()+"|"+emp.getNumOfDeletedLines()+"|"+emp.getFilepath();
            lines.add(s);
            String filetype=emp.getFilepath().toString().substring(emp.getFilepath().toString().lastIndexOf(".")+1);
            s=emp.getDate().toString().trim();
            if(map.containsKey(s)){
            	if(map.get(s).containsKey(filetype)){
            		map.get(s).put(filetype,map.get(s).get(filetype)+emp.getNumOfAddedLines());
            	}
            	else
            		map.get(s).put(filetype, emp.getNumOfAddedLines());
            }
            else{
            	TreeMap<String,Integer> tm=new TreeMap<String, Integer>();
            	tm.put("html", 0);
            	tm.put("xml", 0);
            	tm.put("java", 0);
            	tm.put("js", 0);
            	tm.put("jsp", 0);
            	tm.put("py", 0);
            	tm.put("jq", 0);
            	tm.put("css", 0);
            	tm.put("png", 0);
            	tm.put("ico", 0);
            	tm.put("conf", 0); 

            	
         /*   	tm.put("html", 1);
            	tm.put("xml", 1);
            	tm.put("java", 1);
            	tm.put("js", 1);
            	tm.put("jsp", 1);
            	tm.put("py", 1);
            	tm.put("jq", 1);
            	tm.put("css", 1);
            	tm.put("png", 1);
            	tm.put("ico", 1);
            	tm.put("conf", 1); */

            	tm.put(filetype,emp.getNumOfAddedLines());
            	map.put(s, tm);
            }
         }
         
         for (@SuppressWarnings("rawtypes")
 		Iterator iterator = 
                            Emps.iterator(); iterator.hasNext();){
             SvnObject emp = (SvnObject) iterator.next(); 
             dates.add(emp.getDate().toString());
             added.add(emp.getNumOfAddedLines());
             removed.add(emp.getNumOfDeletedLines());
             filePath.add(emp.getFilepath().toString());
             s = emp.getDate().toString() +"|"+emp.getNumOfAddedLines()+"|"+emp.getNumOfDeletedLines()+"|"+emp.getFilepath();
             lines.add(s);
             String filetype=emp.getFilepath().toString().substring(emp.getFilepath().toString().lastIndexOf(".")+1);
             s=emp.getDate().toString().trim();
             if(map2.containsKey(s)){
             	if(map2.get(s).containsKey(filetype)){
             		map2.get(s).put(filetype,map2.get(s).get(filetype)+emp.getNumOfDeletedLines());
             	}
             	else
             		map2.get(s).put(filetype, emp.getNumOfDeletedLines());
             }
             else{
             	TreeMap<String,Integer> tm2=new TreeMap<String, Integer>();
             	tm2.put("html", 0);
             	tm2.put("xml", 0);
             	tm2.put("java", 0);
             	tm2.put("js", 0);
             	tm2.put("jsp", 0);
             	tm2.put("py", 0);
             	tm2.put("jq", 0);
             	tm2.put("css", 0);
             	tm2.put("png", 0);
             	tm2.put("ico", 0);
             	tm2.put("conf", 0); 

             /*	
             	tm2.put("html", 1);
             	tm2.put("xml", 1);
             	tm2.put("java", 1);
             	tm2.put("js", 1);
             	tm2.put("jsp", 1);
             	tm2.put("py", 1);
             	tm2.put("jq", 1);
             	tm2.put("css", 1);
             	tm2.put("png", 1);
             	tm2.put("ico", 1);
             	tm2.put("conf", 1);  */

             	tm2.put(filetype,emp.getNumOfDeletedLines());
             	map2.put(s, tm2);
             }
          }

         
         
         
         Collections.sort(lines);
         System.out.println(lines);
         System.out.println("******"+map);
         for(int i=0;i<lines.size();i++){
        	 p=(lines.get(i)).indexOf("|");
        	 f=lines.get(i).substring(0,p);
        	 newDates.add(f);
         }
         
        
         
         tx.commit();
      }catch (Exception e) {
         if (tx!=null) tx.rollback();
         e.printStackTrace(); 
      }finally {
         session.close(); 
      }
      FusionChartJSONGenerator fcj = new FusionChartJSONGenerator();
      FusionChartForDeleted fdj=new FusionChartForDeleted();
      try {
    	  if(lr==1)
    		  return fcj.generateJSON(map);
    	  else return fdj.generateJSON(map2);
            }
     
      catch (Exception e) {
		e.printStackTrace();
		return null;
	}     
      
   }
}