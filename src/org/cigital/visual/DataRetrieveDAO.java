package org.cigital.visual;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.cigital.CodeChange.UserDetails;
class DataRetrieveDAO {
	SessionFactory factory;
	// Date Converter
	@SuppressWarnings("rawtypes")
	long dateConverter(String s)throws ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date dt=sdf.parse(s);
		return (dt.getTime()/1000);
		}
	@SuppressWarnings("rawtypes")
	public ArrayList<String> retrieveData(String author,String sdate,String edate){
		ArrayList<String> log=new ArrayList<String>();
		Configuration configuration = new Configuration();
		configuration.configure();
    	factory = new Configuration().configure().buildSessionFactory();
		Transaction tx = null;
    	Session session = factory.openSession();
	    try{
	    	String sql="";
		    tx = session.beginTransaction();
		    sql = "SELECT date,user,mode,filepath FROM SVNLog WHERE user='"+author+"' AND date>='"+sdate+"' AND date<='"+edate+"'";
		    SQLQuery query = session.createSQLQuery(sql);
		    query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		    List data = query.list();
		    for(Object object : data){
		    	Map row = (Map)object;
		        String line=Long.toString(dateConverter(row.get("date").toString()))+"|"+author+"|"+row.get("mode").toString()+"|"+row.get("filepath").toString().trim()+"|\n";
		        log.add(line);
		        }
		    tx.commit();
	    }catch (Exception e) {
	        if (tx!=null) tx.rollback();
	        e.printStackTrace(); 
	    }finally {
	        session.close(); 
	      }
		return log;
	}
}
