package org.cigital.visual;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class VisualizeVideo {
	void visualizeLog(String filename,String browserType){
		try{            
			Runtime rt = Runtime.getRuntime();
			String command="gource -640x480 --hide progress --stop-at-end -o "+filename+".ppm --log-format custom "+filename+".log";
			Process proc = rt.exec(command);
			System.out.println("Visualizing log..");
			proc.waitFor();
			command="ffmpeg -y -r 60 -f image2pipe -vcodec ppm -i "+filename+".ppm -vcodec libx264 -preset ultrafast -pix_fmt yuv420p -crf 1 -threads 0 -bf 0 "+filename+".mp4";
			System.out.println("Converting into video..");
			proc = rt.exec(command);
			proc.waitFor();			
			//if(browserType.equals("firefox")){
				command="ffmpeg -i "+filename+".mp4 -c:v libvpx -crf 10 -b:v 1M -c:a libvorbis "+filename+".webm";
				proc = rt.exec(command);
				proc.waitFor();
			//	}
			command="rm -v "+filename+".ppm";
			proc = rt.exec(command);
			System.out.println("Removing intermediate files..");
			proc.waitFor();
			command="rm -v "+filename+".log";
			proc = rt.exec(command);
			proc.waitFor();
			}
		catch (Throwable t){
			t.printStackTrace();
			}
		}
	String convertToFile(ArrayList<String> A,String author,String path){
		String prefixName=author+Long.toString(System.currentTimeMillis());
		String customName=path+prefixName;
		String filename=customName+".log";
		try {
			FileWriter fr = new FileWriter(filename);
			for(int i=0;i<A.size();i++){
				fr.write(A.get(i));
				}
			fr.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return prefixName;
		}
	}
