package org.cigital.visual;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/VisualizeData")
public class VisualizeData extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public VisualizeData() {
        super();
    }
    public String reverseDate(String s){
    	String modStart[]=s.split("/");
		s=modStart[2]+"-"+modStart[0]+"-"+modStart[1];
		return s;
    }
    public void destroyVideos(String path){
    	try {
    		Runtime rt = Runtime.getRuntime();
    		Process proc;
    		String files[]=(new File(path)).list();
    		for(String s:files){
    			if(s.endsWith(".mp4") || s.endsWith(".webm")){
    				String completePath=path+s;
    				String command="rm -v "+completePath;
    				proc=rt.exec(command);
    				proc.waitFor();
    				}
    		}
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
		String workingDir = getServletContext().getRealPath("/");
		destroyVideos(workingDir);
		response.setContentType("text/html");
		String author=request.getParameter("author"),start=reverseDate(request.getParameter("sd")),end=reverseDate(request.getParameter("ed"));
		String browserType=request.getParameter("browser");
		PrintWriter out = response.getWriter();
		DataRetrieveDAO obj1=new DataRetrieveDAO();
		ArrayList<String> result=new ArrayList<String>();
		try {
			result = obj1.retrieveData(author,start,end);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(result.size()==0){
			out.println("<br><p>The specified user either not found in the database\n or not committed anywork during specified dates..!</p>");
			}
		else{
			String absPath = getServletContext().getRealPath("/");
			VisualizeVideo obj2=new VisualizeVideo();
			String fileName=obj2.convertToFile(result,author,absPath);
			String paraMeter=absPath+fileName;
			obj2.visualizeLog(paraMeter,browserType);
			response.setStatus(307);
			String URL="VideoHome?name="+fileName;
			response.addHeader("Location",URL);
			}
	}

}