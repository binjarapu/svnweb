package org.cigital.visual;


public class SvnObject {
	private int id,numOfAddedLines,numOfDeletedLines,userId;
	private String revisionId,code,date,mode,filepath;
	public SvnObject(int numOfAddedLines, int numOfDeletedLines,
			int userId, String revisionId, String code, String date,
			String mode,String filepath ){
		this.numOfAddedLines=numOfAddedLines;
		this.numOfDeletedLines=numOfDeletedLines;
		this.revisionId=revisionId;
		this.userId=userId;
		this.code=code;
		this.date=date;
		this.mode=mode;
		this.filepath=filepath;
	}
	public SvnObject(){
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumOfAddedLines() {
		return numOfAddedLines;
	}
	public void setNumOfAddedLines(int numOfAddedLines) {
		this.numOfAddedLines = numOfAddedLines;
	}
	public int getNumOfDeletedLines() {
		return numOfDeletedLines;
	}
	public void setNumOfDeletedLines(int numOfDeletedLines) {
		this.numOfDeletedLines = numOfDeletedLines;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getRevisionId() {
		return revisionId;
	}
	public void setRevisionId(String revisionId) {
		this.revisionId = revisionId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getFilepath() {
		return filepath;
	}
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	
}