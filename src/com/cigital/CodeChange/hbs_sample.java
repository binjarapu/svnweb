package com.cigital.CodeChange;
/**
 * 
 * @author VINOD.K
 *
 */
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
public class hbs_sample {
	PrintWriter pw;
	 SessionFactory factory;
	 String username,fromdate,todate;
	/*
	public static void main(String args[]){
		System.out.println("Hello!");
		factory=new Configuration().configure().buildSessionFactory();
	}
	*/
	 public hbs_sample(PrintWriter pw,String username,String fromdate,String todate){
		 this.pw=pw;
		 this.username=username;
		 this.fromdate=fromdate;
		 this.todate=todate;
		 try{
			 factory=new Configuration().configure().buildSessionFactory();
			// addEmployee();
			//pw.println("record inserted");
		 }
		 catch(Exception e){
			 pw.println(e.toString());
		 }
		// pw.println("I am from out side "+this.fromdate+" "+this.todate);
		 if(username.length()>0)	
			 pw.println("<p ><h1 align='center'>"+username+"</p></h1>");
		 else	
			 pw.println("<p ><h1 align='center'>"+"All"+"</p></h1>");
		 listEmployees( );
	 }

	/* Method to CREATE a record in the database */
	   public Integer addEmployee(){
	      Session session = factory.openSession();
	      Transaction tx = null;
	      Integer employeeID = null;
	      try{
	         tx = session.beginTransaction();
	       //  SvnObject record = new SvnObject(10,20,1,"v","v","2015-10-10","v","v");
	       //  employeeID = (Integer) session.save(record); 
	         tx.commit();
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      }finally {
	         session.close(); 
	      }
	      return employeeID;
	   }
	   /* Method to LIST all the record in the table SVNLog */
	   @SuppressWarnings("rawtypes")
	public void listEmployees( ){
		   	  //pw.println("Listing Records..."+fromdate);
		      Session session = factory.openSession();
		      Transaction tx = null;
		      pw.println(" <div class='row'>"+
	        			 "<div class='col-sm-2 btn btn-success' align='center'>Date</div>"+
	        			 "<div class='col-sm-2 btn btn-success' align='center'>Revision ID</div>"+
	        			 "<div class='col-sm-2 btn btn-success' align='center'>No of lines added</div>"+
	        			 "<div class='col-sm-2 btn btn-success' align='center'>No of lines deleted</div>"+
	        			 "<div class='col-sm-2 btn btn-success' align='center'>Modification Type</div>"+
	        			 "<div class='col-sm-2 btn btn-success' align='center'>View Code</div>"+
	        			 "</div><br/>");
		      try{
		    	  String cdd="codedisplay";
		    	  int i=1;
		         tx = session.beginTransaction();
		        // List employees = session.createQuery("FROM SvnObject").list();
		        // pw.println(username);
		       //  pw.println("FROM SvnObject R WHERE R.user='"+username+"' and R.date>="+fromdate+" and R.date<="+todate+" ORDER BY R.date");
		         List employees;
		         if(username.length()>0)
		        	 employees = session.createQuery("FROM SvnObject R WHERE R.user='"+username+"' and R.date>='"+fromdate+"' and R.date<='"+todate+"' ORDER BY R.date").list(); 
		         else
		        	 employees = session.createQuery("FROM SvnObject R WHERE R.date>='"+fromdate+"' and R.date<='"+todate+"' ORDER BY R.date").list(); 
		         //  pw.println("Done");
		        //  pw.println(employees.size());
		          for (Iterator iterator = 
		                           employees.iterator(); iterator.hasNext();){
		        	 cdd="codedisplay"+(i++);
		        	 SvnObject diffo = (SvnObject) iterator.next(); 
		        	 pw.println(" <div class='row'>"+
		        			 "<div class='col-sm-12 btn btn-info' align='center'>"+diffo.getFilepath()+"</div>"+
		        			 "</div>");
		            pw.println("<table class='table-bordered' width='100%'>");
		            pw.println("</table>");
		            String tmp=diffo.getMode(),res="";
		            if(tmp.charAt(0)=='A')
		            	res="Lines Added";
		            else if(tmp.charAt(0)=='R')
		            	res="Lines Removed";
		            else
		            	res="File Modified";
		            	
		            pw.println(" <div class='row'>"+
		        			 "<div class='col-sm-2 btn btn-default' align='center'>"+diffo.getDate()+"</div>"+
		        			 "<div class='col-sm-2 btn btn-default' align='center'>"+diffo.getRevisionId()+"</div>"+
		        			 "<div class='col-sm-2 btn btn-default' align='center'>"+diffo.getNumOfAddedLines()+"</div>"+
		        			 "<div class='col-sm-2 btn btn-default' align='center'>"+diffo.getNumOfDeletedLines()+"</div>"+
		        			 "<div class='col-sm-2 btn btn-default' align='center'>"+res+"</div>"+
		        			 "<div class='col-sm-2 btn btn-default' align='center' id='codetoggle' onClick='myfunc(\""+cdd+"\")'>View Code </div>"+
		        			 "</div>");
		            pw.println(" <div class='row'>"+
		        			 "<div id='"+cdd+"' style='display:none;align:left;margin-left:10px;'><pre><xmp>"+diffo.getCode()+"</xmp></pre></div>"+
		        			 "</div>");
		         }
		         tx.commit();
		      }catch (HibernateException e) {
		         if (tx!=null) tx.rollback();
		         pw.println(e.toString()+ " hi123");
		      }finally {
		         session.close();
		      }
		   }
}
