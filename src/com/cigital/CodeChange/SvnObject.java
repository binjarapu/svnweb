package com.cigital.CodeChange;


public class SvnObject {
	private int numOfAddedLines,numOfDeletedLines,id;
	String user;
	private String revisionId,code,date,mode,filepath;
	public SvnObject(int numOfAddedLines, int numOfDeletedLines,
			String user, String revisionId, String code, String date,
			String mode,String filepath ){
		this.numOfAddedLines=numOfAddedLines;
		this.numOfDeletedLines=numOfDeletedLines;
		this.revisionId=revisionId;
		this.user=user;
		this.code=code;
		this.date=date;
		this.mode=mode;
		this.filepath=filepath;
	}
	public SvnObject(){
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumOfAddedLines() {
		return numOfAddedLines;
	}
	public void setNumOfAddedLines(int numOfAddedLines) {
		this.numOfAddedLines = numOfAddedLines;
	}
	public int getNumOfDeletedLines() {
		return numOfDeletedLines;
	}
	public void setNumOfDeletedLines(int numOfDeletedLines) {
		this.numOfDeletedLines = numOfDeletedLines;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getRevisionId() {
		return revisionId;
	}
	public void setRevisionId(String revisionId) {
		this.revisionId = revisionId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getFilepath() {
		return filepath;
	}
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	
	
}