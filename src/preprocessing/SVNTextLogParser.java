package preprocessing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import model.Author;
import model.Commit;
import model.Diff;

public class SVNTextLogParser {
	public ArrayList<Author> SVNTextLogParser(String filename) throws IOException{
		String data=Read(filename);
		return parser(data);// check..
	}
	public static String Read(String val) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(new File(val)));
		String everything ="";
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            sb.append(System.lineSeparator());
	            line = br.readLine();
	        }
	        everything = sb.toString();
	    } finally {
	        br.close();
	    }
	    return everything;
	}
	public static ArrayList<Author> parser(String input){
		HashMap<String,Author> nameData=new HashMap<String, Author>();
				String[] Data=input.split("------------------------------------------------------------------------");
				boolean firstNo=false;
				for(String s:Data){
					if(firstNo){
					String now[]=s.split("\n");
					int ind=0;
					boolean ok=false;
					for(;ind<now.length;ind++)
						if(now[ind].split("\\|").length==4){
						ok=true;
							break;
						}
					if(ok){
					String fields[]=now[ind].split("\\|");
					String tempAuthor=fields[1].trim();
					String tempId=fields[0];
					String tempDate=fields[2];
					tempDate=tempDate.trim().split("\\+")[0].trim().split(" ")[0].trim();
					/*Commit Class*/
					Commit temp_commit=new Commit();
					temp_commit.setRefno(tempId);
					temp_commit.setTimes(tempDate);
					
					for(int i=ind+1;i<now.length;i++){
						
						//Description of lines
								for(;i<now.length;i++){
									if(now[i].startsWith("Index:")){
										break;
									}
									else;
								}
						//diff Class path,code
								ArrayList<Diff> diff=new ArrayList<Diff>();
								Diff tempDiff = null;
								boolean firstTime=false;
								int indCount=0,creCount=0;
								for(;i<now.length;i++){
									if(now[i].startsWith("Index:")){
										if(firstTime){
											diff.add(tempDiff);
											creCount++;
										}
										indCount++;
										tempDiff=new Diff();
										tempDiff.setPath(now[i].substring(6));
										tempDiff.setCode(" ");
									}
									else{
										String str=tempDiff.getCode();
										tempDiff.setCode(str+"\n"+now[i]);
									//	System.out.println("(****"+tempDiff.getCode()+"\n(**)\n"+now[i]+"***)");
										firstTime=true;
									}
									/*if(indCount!=creCount)
									diff.add(tempDiff);*/
								}
								if(indCount!=creCount)
									diff.add(tempDiff);
							temp_commit.setDifff(diff);
					}
					if(nameData.containsKey(tempAuthor)){
						nameData.get(tempAuthor).getCommits().add(temp_commit);
					}
					else{
						/*Author Class*/
						Author temp=new Author();
						temp.setName(tempAuthor);
						ArrayList<Commit> commitsList=new ArrayList<Commit>();
						commitsList.add(temp_commit);
						temp.setCommits(commitsList);
						nameData.put(tempAuthor,temp);
					}
					}
				}
					else
						firstNo=true;
				}
				//Make Authors ArrayList
				ArrayList<Author> returnAuthors=new ArrayList<Author>();
				Iterator<String> k=nameData.keySet().iterator();
				while(k.hasNext()){
					returnAuthors.add(nameData.get(k.next()));
				}
		return returnAuthors;
	}
}