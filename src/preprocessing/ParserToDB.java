package preprocessing;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;

import model.Author;
import model.Commit;
import model.Diff;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.cigital.CodeChange.SvnObject;

public class ParserToDB {
	SessionFactory factory;
	Session session;
	public void insertIntoDB(String path){
		  try{
	          factory = new Configuration().configure().buildSessionFactory();
	       }catch (Throwable ex) { 
	          System.err.println("Failed to create sessionFactory object." + ex);
	          throw new ExceptionInInitializerError(ex); 
	       }
	      session = factory.openSession();
	      
		ArrayList<Author> authorsList=null;
		try {
			authorsList = new SVNTextLogParser().SVNTextLogParser(path);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Iterator<Author> iterateAuthors=authorsList.iterator();
		while(iterateAuthors.hasNext()){
			Author tempP=iterateAuthors.next();
			//System.out.println("Author Name::"+tempP.getName());
			Iterator<Commit> tempC=tempP.getCommits().iterator();
			while(tempC.hasNext()){
				Commit tempC1=tempC.next();
				//System.out.println(tempC1.getRefno());
				//System.out.println(tempC1.getTimes());
				Iterator<Diff> tempD=tempC1.getDifff().iterator();
				while(tempD.hasNext()){
						Diff tempD1=tempD.next();
						//System.out.println("File Path:: "+tempD1.getPath());
						//System.out.println("Diff:: "+tempD1.getCode());
						String code[]=tempD1.getCode().split("\n");
						int add=0,rem=0;
						for(String s:code){
							if(s.trim().startsWith("+")&&!s.trim().startsWith("++"))
								add++;
							if(s.trim().startsWith("-")&&!s.trim().startsWith("--"))
								rem++;
						}
						int id=1;
						String str=tempP.getName().trim();
						/*if(str.equals("ayesha"))
							id=1;
							else if(str.equals("namdev"))date,mode,tempD1.getPath()
							id=2;
							else if(str.equals("giridhar"))
							id=3;
							else if(str.equals("amit"))
							id=4;
							else if(str.equals("varadaraj"))
							id=5;
							else if(str.equals("arjun"))
							id=6;
							else if(str.equals("ram"))
							id=7;*/
						char mode='N';
						if(add==0)
							mode='D';
						else if(rem==0)
							mode='A';
						else
							mode='M';
						
						//System.out.println(tempC1.getRefno() + tempD1.getCode() +  add +  rem + id + tempC1.getTimes() +  mode + tempD1.getPath());
						SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
						Date date=null;
						try {
							date = format1.parse(tempC1.getTimes());
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							SvnObject record = new SvnObject(add,rem,tempP.getName()+"",tempC1.getRefno(),new SerialBlob(tempD1.getCode().getBytes()).toString(),format1.format(date).toString(),mode+"",tempD1.getPath());
							insert(record);
						} catch (SerialException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				
				
				}
			}
		}
	}
	private void insert(SvnObject slm){
	Session session = null;
	session = factory.openSession();
    Transaction tx = null;
      try{
          tx = session.beginTransaction();
          session.save(slm); 
          tx.commit();
       }catch (HibernateException e) {
          if (tx!=null) tx.rollback();
          e.printStackTrace(); 
       }finally {
          session.close(); 
       }
	}
}
