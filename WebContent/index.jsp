<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/jqueryJs/jquery-ui.css">
<link rel="stylesheet" href="/gridJs/css/ui.jqgrid.css">
<script src="/jqueryJs/external/jquery/jquery.js"></script>
<script src="/gridJs/js/i18n/grid.locale-en.js"></script>
<script src="/gridJs/js/jquery.jqGrid.src.js"></script>
<script src="/jqueryJs/jquery-ui.js"></script>
<script src="/js/getJqGridData.js"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <!-- Load jQuery JS -->
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <!-- Load jQuery UI Main JS  -->
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <script>
	  /*  jQuery ready function. Specify a function to execute when the DOM is fully loaded.  */
	  $(document).ready(
	    /* This is the function that will get executed after the DOM is fully loaded */
	    function () {
	      $( "#datepicker_from" ).datepicker({
	        changeMonth: true,//this option for allowing user to select month
	        changeYear: true //this option for allowing user to select from year range
	      });
	      $( "#datepicker_to" ).datepicker({
		        changeMonth: true,//this option for allowing user to select month
		        changeYear: true //this option for allowing user to select from year range
		      });
	    }
	  );
	  $(document).ready(
			    /* This is the function that will get executed after the DOM is fully loaded */
			    function () {
			      $("#notification").hide();
			      $("#fusionhead").hide();
			    }
			  );
  </script>
  <script type="text/javascript" >
  function myfunc(k){
	  var idd="#"+k;
	  $(idd).toggle(1000);
  }
    $(function() {
    	$("#updatedb").click(function(e){
    		var formElement = $("[name='fileuploadform']")[0];
    		var fd = new FormData(formElement); 
    		var fileInput = $("[name='uploadfile']")[0];
    	    if(fileInput==null){
    			alert("Please select a file..");
    			}
    		else{
    			fd.append('file', fileInput.files[0] );
    			console.log(fd);
    		    $.ajax({
    				url: 'UpdateDatabase',
    		        data: fd,
    		        processData: false,
    		        contentType: false,
    		        type: 'POST',
    		        success: function(data,textStatus, jqXHR){
    					document.getElementById('notification').innerHTML="Updated !"+data+" rows added.";
    					$('#notification').show(0);
    					$('#notification').fadeOut(5000);
    					$('#notification').hide(5000);
    					}
    				});
    			}
    		});
    	$("#cleardb").click(function(e){
    		$.ajax({
                type: "GET",
                url: "clearDb",
                success: function(data,textStatus, jqXHR){
                	document.getElementById('notification').innerHTML="Cleared "+data+" rows";
                    $('#notification').show(0);
                    $('#notification').fadeOut(5000);
                    $('#notification').hide(5000);
                    //alert(data);
               }
            });
    	});
        $("#Submit").click(function(e) {
        	//document.getElementById('response').innerHTML ="Loading...";
            var uname = $("#ex1").val();
            var fromdate = $("#datepicker_from").val();
            var todate = $("#datepicker_to").val();
            var types = document.getElementsByName("radio");
			var reqtype="plain"
            for(var i = 0; i < types.length; i++) {
               if(types[i].checked == true) {
                   reqtype=types[i].value;
               }
             }
			//alert(reqtype);
			var urls="hbs_servlet";
			if(reqtype=="fusion"){
				$("#response").hide();
				$("#fusionhead").show();
				$("#fusion").show();
				var dataString = 'uname='+ uname + '&fromdate=' + fromdate + '&todate=' + todate + '&rtype=' + reqtype ;
				$("#fusionl").load("fs.jsp?"+dataString);
				$("#fusionr").load("fsright.jsp?"+dataString);
			}
			else if(reqtype=="animation"){
				$("#fusionhead").hide();
				$("#fusion").hide();
				$("#response").show();
				var dataString = 'author='+ uname + '&sd=' + fromdate + '&ed=' + todate + '&rtype=' + reqtype ;
				$("#response").load("VisualizeData?"+dataString);
			}
			else{
				$("#fusionhead").hide();
				$("#fusion").hide();
				$("#response").show();
				var dataString = 'uname='+ uname + '&fromdate=' + fromdate + '&todate=' + todate + '&rtype=' + reqtype ;
            	$.ajax({
                    type: "POST",
                    url: "hbs_servlet",
                    data: dataString,
                    success: function(data, textStatus, jqXHR){
                    	document.getElementById('response').innerHTML=data;
                        $('.success').fadeIn(200).show();
                        $('.error').fadeOut(200).hide();
                    }
                });
			}
            return false;
        });
        $("#displaycode").click(function(e) {
        	alert("hi");
        });
        
    });
    </script>
  
<title>SVN</title>
</head>
<body>
	<div class="btn btn-info" style="width:100%;">
		<h1 align="center">SVN Analysis</h1>
				<div class="form-group">
			      <div class="col-xs-4">
			        <form action='' name='fileuploadform' method='post'>
			        <div style="float:left;">
			        	<input type='file' name='uploadfile' class='btn btn-primary'>
			        </div>	
			        <div style="margin-left:10px;">
			        	<input style="margin-left:10px;" class='btn btn-warning' type="button" name="updatedb" value="Update Database" id='updatedb'>
			       </div>
			        </form>
			      </div>
			      <div class="col-xs-4">
			        <div id="notification" align='center' class='btn btn-danger'></div>
			      </div>
			      <div class="col-xs-4">
			        <input class='btn btn-danger' type="button" name="cleardb" value="Clear Database" id='cleardb'>
			      </div>
			    </div>
		
		<div >
			
		</div>
	</div>
	<div align="center">
			<form role="form" align="center" style="margin-left:75px;" method="post" >
			    <div class="form-group">
			      <div class="col-xs-3">
			        <input class="form-control" id="ex1" type="text" placeholder="User Name" name="uname" required >
			      </div>
			      <div class="col-xs-3">
			        <input class="form-control" id="datepicker_from" type="text" placeholder="From Date" name="FromDate" required>
			      </div>
			      <div class="col-xs-3">
			        <input class="form-control" id="datepicker_to" type="text" placeholder="To Date" name="ToDate" required> 
			      </div>
			      <div class="col-xs-3" style="margin-left:-70px">
			     	<input type="submit" class="btn btn-primary" value="View" id="Submit"/>
			      </div>
			    </div>
			    <form id="radiof">
			    <div class="form-group">
			    <br/>
			    <br/><br/>
			      <div class="col-xs-2">
			        <label class="radio-inline">
				      <input type="radio" name="radio" checked="checked" value="plain">Code Change
				    </label>
			      </div>
			      <div class="col-xs-2">
			        <label class="radio-inline">
				      <input type="radio" name="radio" value="fusion">Code Velocity
				    </label>
			      </div>
			      <div class="col-xs-2">
			       <label class="radio-inline">
				      <input type="radio" name="radio" value="animation">Visualize
				    </label>
			      </div>
			    </div>
			    </form>
	  		</form>
	</div>
	<br/>
	<div style="clear:both;"></div>
	<br/>
	<div>
		<div id="response">
		</div>
		<div class="form-group" id="fusionhead">
			    <div class="col-xs-6 btn btn-primary" align="center">
			    	Added Lines of Code
			    </div>
			    <div class="col-xs-6 btn btn-primary" align="center">
			       Deleted Lines of Code
			    </div>
		</div>
		<div class="form-group" id="fusion">
			    <div class="col-xs-6" id="fusionl">
			    </div>
			    <div class="col-xs-6" id="fusionr">
			    </div>
		</div>
	</div>
</body>
</html>