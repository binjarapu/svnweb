jQuery(document).ready(function() {
	$("#list").jqGrid({
		url : "GridServlet",
		datatype : "json",
		mtype : 'POST',
		colNames : [ 'FileName', 'RevNo', 'Time', 'Type', 'Code','Author' ],
		colModel : [ {
			name : 'fileName',
			index : 'fileName',
			width : 150,
			editable : true
		}, {
			name : 'revNo',
			index : 'revNo',
			width : 150,
			editable : true
		}, {
			name : 'time',
			index : 'time',
			width : 150,
			editable : true
		}, {
			name : 'type',
			index : 'type',
			width : 80,
			editable : true
		}, {
			name : 'code',
			index : 'code',
			width : 100,
			editable : true
		},{
			name : 'authorName',
			index : 'authorName',
			width : 100,
			editable : true
		} ],
		pager : '#pager',
		rowNum : 10,
		rowList : [ 10, 20, 30 ],
		//sortname : 'invid',
	//	sortorder : 'desc',
		viewrecords : true,
		gridview : true,
		caption : 'Display SVNLog',
		jsonReader : {
			repeatitems : false,
		},
		editurl : "GridServlet"
	});

	jQuery("#list").setGridParam({rowNum:10}).trigger("reloadGrid");
	jQuery("#list").jqGrid('navGrid', '#pager', {
		edit : true,
		add : true,
		del : true,
		search : true
	});
});